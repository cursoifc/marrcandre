#!/usr/bin/env python
#coding: utf-8

__author__ = 'marco'

from django.db.models.base import ModelBase
from django.db.models.fields import CharField, IntegerField

class Categoria(ModelBase):

    nome = CharField(u"Nome", max_length=100, unique=True)
    ordem = IntegerField(u"Ordem", default=0, help_text=u"Ordene como se fosse uma fila: números menores têm prioridade.")

    class Meta:
        verbose_name = u"Categoria"
        verbose_name_plural = u"Categorias"
        ordering = ["ordem"]
        app_label = "servicos"

    def __unicode__(self):
        return self.nome
